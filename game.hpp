#ifndef INCLUDED_GAME
#define INCLUDED_GAME

#include "game.hpp"
#include <QMetaType>
#include <QDebug>
#include <algorithm>
#include <limits>
#include <cmath>
#include <cassert>

struct Point{
    static constexpr float nan = std::numeric_limits<float>::quiet_NaN();
    float x,y;

    Point(float sx, float sy) :x(sx), y(sy) { }
    Point() :Point(nan,nan) { }
    static Point zero() { static Point zp(0.f,0.f); return zp; }

    Point operator+(const Point& p) const { return Point(x+p.x, y+p.y); }
    Point operator-(const Point& p) const { return Point(x-p.x, y-p.y); }
    bool operator==(const Point& p) const { return x==p.x && y==p.y; }
    bool operator!=(const Point& p) const { return !(*this==p); }
    Point& operator+=(const Point& p) { y+=p.y; x+=p.x; return *this; }
    Point& operator-=(const Point& p) { y-=p.y; x-=p.x; return *this; }

    float distpow(const Point& p) const {
        float dx = x-p.x, dy = y-p.y;
        return dx*dx + dy*dy;
    }
    float dist(const Point& p) const {
        return sqrtf(distpow(p));
    }
    void fix(const float absmax) {
        if(x> absmax) x =  absmax;
        if(x<-absmax) x = -absmax;
        if(y> absmax) y =  absmax;
        if(y<-absmax) y = -absmax;
    }
};
Q_DECLARE_METATYPE(Point)
Q_DECLARE_METATYPE(std::vector<Point>)

struct Player{
    Point point, velocity;
    Player() : point(), velocity() {}
    Player(const Point& p, const Point& vel) : point(p), velocity(vel) {}
    size_t hash() const {
        //std::hash<size_t> fh;
        union fs { float f; size_t s; };
        fs fss[] = {
            point.x,
            point.y,
            velocity.x,
            velocity.y
        };
        auto fh = [](size_t a) {
            return (a<<13) | (a>>(64-13));//(a<<1) ^ (a>>63) ^ (size_t)0x8939318A1919DEEE;
        };
        return fh(fss[0].s ^
                fh(fss[1].s ^
                 fh(fss[2].s ^
                  fh(fss[3].s))));
    }
};

class Obstacle {
public:
    Obstacle() = default;
    virtual float y(float x) const {return -8931919.f;}
    virtual bool isInside(const Point& p) const {return false;}
};

class Node {
private:
    static constexpr float uv = 1.25;
    static constexpr float unitvel[] = {-uv, 0.f, uv};
    static constexpr float maxvel = uv*20;       //ある軸方向の最大速度

    static Point goal;  //h'の計算をここに閉じ込めたかったのでここに置いた
    Player ply;
    size_t id, parentid;
    int g;
    float hdash;
public:
    const Player& getPlayer() const { return ply; }
    size_t getId() const { return id; }
    size_t getParentId() const { return parentid; }
    float getEvalval() const { return g+hdash; }
    bool isGoal() const { return ply.point.x >= goal.x; }
    static bool isGoal(const Player& p) { return p.point.x >= goal.x; }
    bool isRoot() const { return id == 0 && parentid == 0; }
private:
    static float calchdash(const Player& ply) {
        if(isGoal(ply)) return 0;
        return ply.point.dist(goal) / maxvel;
    }
    Node(const Player ply) :
            ply(ply.point,Point::zero()),
            id(0), parentid(0),                //root dake id == parentid == 0
            g(0), hdash(calchdash(ply)) {}
    Node(const Node& parent, const Point& dacc) :
            parentid(parent.id),
            g(parent.g + 1) {
        Point tmpv = parent.ply.velocity + dacc;
        tmpv.fix(maxvel);
        ply = Player(parent.ply.point + tmpv, tmpv);
        id = ply.hash();
        assert(ply.point.x>=0 || id!=0);
        hdash = calchdash(ply);
    }
public:
    Node() = default;
    typedef std::array<Node, 5> NodeChild;
    static void setGoal(const Point& p) { Node::goal = p; }
    static Node getRoot(const Point& p) { return Node(Player(p,Point(0,0))); }
    NodeChild getChild() const {
        static Point cache[] = {
            Point(+uv,  0),
            Point(  0,+uv),
            Point(  0,-uv),
            Point(  0,  0),
            Point(-uv,  0),
        };
        NodeChild nc;
        //for(int i=0; i<3; i++)
        //for(int j=0; j<3; j++) {
        //    int wa = i+j;
        //    if(wa!=4 && !(wa&1))
        //        continue;
        //    Point tmpp(Node::unitvel[i],Node::unitvel[j]);
        //    nc[i*3 + j] = Node(*this, tmpp);
        //}
        for(int i=0; i<4; i++)
            nc[i] = Node(*this,cache[i]);
        return nc;
    }
};

#endif
