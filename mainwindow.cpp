#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "runner.hpp"
#include <vector>
#include <cmath>
#include <QThread>
#include <QTimer>
#include <QString>

std::vector<Obstacle*> obstacles;
Point start,goal;

QThread* worker = nullptr;
Base* searcher = nullptr;
QTimer *timer = nullptr;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    qRegisterMetaType<Point>("Point");
    qRegisterMetaType<std::vector<Point>>("std::vector<Point>");

    //obstacle class
    //TODO: ituka delete shiyou na
    //obstacles.push_back(new Lobstacle1(370,false));
    //obstacles.push_back(new Lobstacle1(430,true));
    start = Point(0,400);
    goal = Point(800,400);
    Node::setGoal(goal);
    ui->openGLWidget->setObstacle(obstacles);

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, ui->openGLWidget, &GLWidget::repainter);
    on_pushButton_4_clicked();
    timer->start(100);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::startSearch(Base* ins,QTextEdit* te) {
    if(worker != nullptr) {
        if(searcher->isSearching()) {
            qDebug() << "searcher is still living!!";
            return;
        }
        worker->quit();
        worker->wait();
        delete searcher;
        delete worker;
        worker = nullptr;
        searcher = nullptr;
        ui->openGLWidget->clearPoints();
    }
    worker = new QThread;
    searcher = ins;
    qDebug() << "onprogress" << connect(searcher,&Base::onProgress,[&](Point p) {
        //qDebug() << p.x << p.y;
        ui->openGLWidget->addPoint(p,false);
    });
    qDebug() << "onfinish" << connect(searcher,&Base::onFinish,this,[&,te](std::vector<Point> path,int g, float b) {
        qDebug() << "finished:" << path.size() << endl;
        for(auto& p : path) {
            qDebug() << p.x << p.y << endl;
            ui->openGLWidget->addPoint(p,true);
        }
        QString str = QString("g  = %1\nb* = %2").arg(g).arg(b);
        te->setHtml(str);
        //searcher = nullptr;
        //dareka ni delete saseru
    },Qt::QueuedConnection);
    searcher->moveToThread(worker);
    worker->start();
    QMetaObject::invokeMethod(searcher, "searchstart");
}

void MainWindow::on_pushButton_clicked()   { startSearch(new habatan(obstacles,start,goal),ui->textEdit); }
void MainWindow::on_pushButton_2_clicked() { startSearch(new fukatan(obstacles,start,goal),ui->textEdit_2); }
void MainWindow::on_pushButton_3_clicked() { startSearch(new   astar(obstacles,start,goal),ui->textEdit_3); }

bool MainWindow::updateobsbefore() {
    if(searcher!=nullptr && searcher->isSearching())
        return false;
    timer->stop();      //make sure not to used obstacles by glwidget
    //ui->openGLWidget->setObstacle();
    for(auto& v : obstacles)
        delete v;
    obstacles.clear();
    return true;
}
void MainWindow::updateobsafter(){
    ui->openGLWidget->setObstacle(obstacles);
    ui->openGLWidget->clearPoints();
    timer->start();
}

void MainWindow::on_pushButton_4_clicked() {
    if(!updateobsbefore())
        return;
    obstacles.push_back(new Lobstacle2(370,false));
    obstacles.push_back(new Lobstacle2(430,true));
    updateobsafter();
}

void MainWindow::on_pushButton_5_clicked() {
    if(!updateobsbefore())
        return;
    obstacles.push_back(new Lobstacle1(370,false));
    obstacles.push_back(new Lobstacle1(430,true));
    updateobsafter();
}

void MainWindow::on_pushButton_6_clicked() {
    if(!updateobsbefore())
        return;
    float xoff = 0;
    float yoff = 40;
    obstacles.push_back(new Lobstacle1(yoff+280,false,0.5,80));
    obstacles.push_back(new Lobstacle1(yoff+470,true,1.3,70));
    obstacles.push_back(new Lobstaclec(Point(xoff+360,yoff+360),90));

    obstacles.push_back(new Lobstaclec(Point(xoff+490,yoff+290),30));
    obstacles.push_back(new Lobstaclec(Point(xoff+500,yoff+430),40));
    updateobsafter();

}
