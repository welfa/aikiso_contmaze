#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include <vector>
#include <QCoreApplication>
#include "game.hpp"

class GLWidget : public QGLWidget{
    Q_OBJECT
private:
    static constexpr int GridNum = 80;
    static constexpr int GridLength = 10;
    static constexpr int WinW = GridNum*GridLength+1;
    static constexpr int WinH = GridNum*GridLength+1;
    static constexpr int PaddingH = 3;
    std::vector<Obstacle*> obstacle;
    std::array<float, 2*4*(GridNum+1)> gridPoints;
    std::array<unsigned short, 2*2*(GridNum+1)> gridPointsIndex;
    std::vector<Point> pointss[2];
    void Draw();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
public:
    explicit GLWidget(QWidget *parent = 0) : QGLWidget(parent) {}
public slots:
    void repainter() {
        if(QCoreApplication::hasPendingEvents())
            return;
        repaint();
    }
    void addPoint(const Point& p, bool ans) {
        auto& points = pointss[ans?1:0];
        if(!points.empty() && *(points.rbegin()) == p)
            return;
        points.push_back(p);
    }
    void setObstacle(const std::vector<Obstacle*>& obs) { obstacle = obs; }
    void clearPoints() { for(auto& v : pointss) v.clear(); }
};

#endif // GLWIDGET_H
