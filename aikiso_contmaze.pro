#-------------------------------------------------
#
# Project created by QtCreator 2015-05-15T21:34:49
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = aikiso_contmaze
TEMPLATE = app
CONFIG += c++11


SOURCES += main.cpp\
        mainwindow.cpp\
        game.cpp\
        glwidget.cpp

HEADERS  += mainwindow.h\
        game.hpp\
        runner.hpp\
        glwidget.h\

FORMS    += mainwindow.ui
