#ifndef INCLUDED_RUNNER
#define INCLUDED_RUNNER

#include "game.hpp"
#include <algorithm>
#include <vector>
#include <deque>
#include <stack>
#include <map>
#include <QMetaType>
#include <QDebug>
#include <cassert>
#include <cmath>

class Base : public QObject {
    Q_OBJECT
protected:
    const std::vector<Obstacle*> obstacle;
    const Point start,goal;
    bool searching = false;
    float calcbstar(int N, int d) {
       float poi = 1;
       const float sa = 1.f/256.f;
       while(true) {
           poi += sa;
           float tmp = (powf(poi,d+1)-1) / (poi-1);
           if(N+1 <= tmp)
               break;
       }
       return poi;
    }

public:
    Base(const std::vector<Obstacle*>& o,
        const Point& _start,
        const Point& _goal) : obstacle(o), start(_start), goal(_goal) { }
    bool isSearching() const { return searching; }
public slots:
    virtual void searchstart() {//qDebug() << "something went wrong" << endl;
    }
signals:
    void onProgress(Point);
    void onFinish(std::vector<Point>,int,float);
};


class habatan : public Base {
    Q_OBJECT
public:
    habatan(const std::vector<Obstacle*>& o,
        const Point& start,
        const Point& goal) : 
            Base(o, start, goal) { }
public slots:
    void searchstart() {
        searching = true;
        //qDebug() << "search method invoked." << endl;
        std::deque<Node> open;
        std::map<size_t, Node> closed;
        open.push_back(Node::getRoot(start));                   //1
        while(!open.empty()) {                                  //2
            Node target = open.front();                        //3
            if(closed.count(target.getId())>0) {
                open.pop_front();   //target destroyed
                continue;
            }
            if(target.isGoal())                                 //4
                break;
            open.pop_front();   //target destroyed
            emit onProgress(target.getPlayer().point);
            closed[target.getId()] = target;
            auto children = target.getChild();                  //5
            std::for_each(children.cbegin(), children.cend(), [&](const Node& c) {
                //有効でないノード以外追加
                if(!std::all_of(obstacle.begin(), obstacle.end(),
                        [&](const Obstacle* o) {
                            return o->isInside(c.getPlayer().point);
                        })) {
                    //qDebug() << "out of maze";
                    return;
                }
                //x is nondecreasing..
                if(c.getPlayer().velocity.x < 0.f) {
                    //qDebug() << "x decreasing node";
                    return;
                }
                //ignore existing Node..
                //TODO: overwrite better Node
                if(closed.count(c.getId())>0) {
                    //qDebug() << "edakari!" << c.getId();
                    return;
                }
                //|velocity| == 0 dame
                if(c.getPlayer().velocity.distpow(Point::zero())<(1/1024.f))
                    return;
                open.push_back(c);
            });
        }
        std::vector<Point> path;
        if(open.empty()) {
            emit onFinish(path,0,-1);                 //失敗(空パス返して終了)
            searching = false;
            return;
        }
        Node* node = &open.front();
        while(true) {
            path.push_back(node->getPlayer().point);
            if(node->isRoot())
                break;
            assert(closed.count(node->getParentId())==1 && "mettya are");
            node = &(closed.at(node->getParentId()));
        }
        emit onFinish(path,path.size(),calcbstar(closed.size(),path.size()-1));
        searching = false;
    }
};

class fukatan : public Base {
    Q_OBJECT
public:
    fukatan(const std::vector<Obstacle*>& o,
        const Point& start,
        const Point& goal) :
            Base(o, start, goal) { }
public slots:
    void searchstart() {
        searching = true;
        //qDebug() << "search method invoked." << endl;
        std::deque<Node> open;
        std::map<size_t, Node> closed;
        open.push_back(Node::getRoot(start));                   //1
        while(!open.empty()) {                                  //2
            Node target = open.back();                        //3
            if(closed.count(target.getId())>0) {
                open.pop_back();   //target destroyed
                continue;
            }
            if(target.isGoal())                                 //4
                break;
            open.pop_back();   //target destroyed
            emit onProgress(target.getPlayer().point);
            closed[target.getId()] = target;
            auto children = target.getChild();                  //5
            std::for_each(children.crbegin(), children.crend(), [&](const Node& c) {
                //fukatan ha ketsu kara totteku node rbegin
                //有効でないノード以外追加
                if(!std::all_of(obstacle.begin(), obstacle.end(),
                        [&](const Obstacle* o) {
                            return o->isInside(c.getPlayer().point);
                        })) {
                    //qDebug() << "out of maze";
                    return;
                }
                //x is nondecreasing..
                if(c.getPlayer().velocity.x < 0.f) {
                    //qDebug() << "x decreasing node";
                    return;
                }
                //ignore existing Node..
                //TODO: overwrite better Node
                if(closed.count(c.getId())>0) {
                    //qDebug() << "edakari!" << c.getId();
                    return;
                }
                //|velocity| == 0 dame
                if(c.getPlayer().velocity.distpow(Point::zero())<(1/1024.f))
                    return;
                open.push_back(c);
            });
        }
        std::vector<Point> path;
        if(open.empty()) {
            emit onFinish(path,0,-1);                 //失敗(空パス返して終了)
            searching = false;
            return;
        }
        Node* node = &open.back();
        while(true) {
            path.push_back(node->getPlayer().point);
            if(node->isRoot())
                break;
            assert(closed.count(node->getParentId())==1 && "mettya are");
            node = &(closed.at(node->getParentId()));
        }
        emit onFinish(path,path.size(),calcbstar(closed.size(),path.size()-1));
        searching = false;
    }
};


class astar : public Base {
    Q_OBJECT
public:
    astar(const std::vector<Obstacle*>& o,
        const Point& start,
        const Point& goal) :
            Base(o, start, goal) { }
public slots:
    void searchstart() {
        searching = true;
        //qDebug() << "search method invoked." << endl;
        std::multimap<float, Node> open;
        std::map<size_t, Node> closed;
        auto rnode = Node::getRoot(start);
        open.emplace(rnode.getEvalval(), rnode);                   //1
        while(!open.empty()) {                                  //2
            const auto oit = open.cbegin();
            const Node target = (*oit).second;                        //3 kono ato mo target sawaru node & kinshi
            if(closed.count(target.getId())>0) {
                //isGoal no ato to koko ni aru hitsuyou ga aru
                open.erase(oit);
                continue;
            }
            if(target.isGoal())                                 //4
                break;
            open.erase(oit);
            emit onProgress(target.getPlayer().point);
            closed[target.getId()] = target;
            auto children = target.getChild();                  //5
            std::for_each(children.crbegin(), children.crend(), [&](const Node& c) {
                //有効でないノード以外追加
                if(!std::all_of(obstacle.begin(), obstacle.end(),
                        [&](const Obstacle* o) {
                            return o->isInside(c.getPlayer().point);
                        })) {
                    //qDebug() << "out of maze";
                    return;
                }
                //x is nondecreasing..
                if(c.getPlayer().velocity.x < 0.f) {
                    //qDebug() << "x decreasing node";
                    return;
                }
                //ignore existing Node..
                //TODO: overwrite better Node
                if(closed.count(target.getId())>0) {
                    //onaji ichi ni iru (hyokati ga onaji toha itte inai)
                    if(closed[target.getId()].getEvalval() < target.getEvalval()) {
                        //qDebug() << "edakari!" << c.getId();
                        return;
                    }
                }
                //|velocity| == 0 dame
                if(c.getPlayer().velocity.distpow(Point::zero())<(1/1024.f))
                    return;
                open.emplace(c.getEvalval(),c);
            });
        }
        std::vector<Point> path;
        if(open.empty()) {
            emit onFinish(path,0,-1);                 //失敗(空パス返して終了)
            searching = false;
            return;
        }
        Node const * node = &(*(open.cbegin())).second;
        while(true) {
            path.push_back(node->getPlayer().point);
            if(node->isRoot())
                break;
            assert(closed.count(node->getParentId())==1 && "mettya are");
            node = &(closed.at(node->getParentId()));
        }
        emit onFinish(path,path.size(),calcbstar(closed.size(),path.size()-1));
        searching = false;
    }
};


#endif
