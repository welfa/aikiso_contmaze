#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <cmath>
#include "runner.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

private:
    Ui::MainWindow *ui;
    void startSearch(Base* ins, QTextEdit* te);
//  void startSearch(Base*);
    void updateobsafter();
    bool updateobsbefore();
};

class Lobstacle1 : public Obstacle {
public:
    float offset;
    bool isInsideAbove;
    float iso, amp;
    Lobstacle1(float off, bool isia, float iso=2, float amp=120) :
        offset(off), isInsideAbove(isia), iso(iso), amp(amp) {}
    float y(float x) const {
        auto tmp = -sin(x*2*iso*M_PI/800)*amp+ offset;
        return tmp;
    }
    bool isInside(const Point &p) const {
//      if(p.x>800) return true;
        auto tmp = (p.y > y(p.x));
        return tmp ^ isInsideAbove;
    }
};
class Lobstacle2 : public Obstacle {
public:
    float offset;
    bool isInsideAbove;
    Lobstacle2(float off, bool isia) : offset(off), isInsideAbove(isia) {}
    float y(float x) const {
        return offset;
    }
    bool isInside(const Point &p) const {
//      if(p.x>800) return true;
        auto tmp = (p.y > y(p.x));
        return tmp ^ isInsideAbove;
    }
};

class Lobstaclec : public Obstacle {
public:
    Point center;
    float r;
    Lobstaclec(const Point& c, float r) : center(c), r(r) {}
    float y(float x) const {
        if(x<=center.x-r || center.x+r<=x)
            return nanf("");
        return (((int)x&1) ? -1 : 1) * (sqrtf(r*r - powf(x-center.x,2))) + center.y;
    }
    bool isInside(const Point &p) const {
        return p.dist(center)>=r;
    }
};
#endif // MAINWINDOW_H
